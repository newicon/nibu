/**
 * This file controls the command line interface for nibu
 * but has no explicit backup functionality itself.
 * The brains is in ../backup.js nibu object.
 * 
 * This forms the abstraction between the interface and the functionality
 * @see http://visionmedia.github.io/commander.js
 */

var nibu = require('../backup.js');
var _ = require('underscore');
var program = require('commander');
var colors = require('colors');
var pkg = require('../package.json');
var fs = require('fs');
var promptly = require('promptly');
var pkg = require('../package.json');

var updateNotifier = require('update-notifier');
// Check for newer version of Nibu
var notifier = updateNotifier({
	pkg: pkg
});
if (notifier.update) {
	notifier.notify();
}

if (!nibu.hasConfig()) {
	console.log(nibu.configError());
}

process.on('uncaughtException', function(err) {
	var message = err.stack;
	console.log('An error occured!'.red);
	console.log(err);
	process.exit(1);
});

program
	.version(pkg.version)
	.usage('[command|options]');

program
	.command('all')
	.description('\n\
	Backup everything configured (media and mysql)'.cyan)
	.action(function() {
		nibu.uploadAll();
	});

// nibu media [method] [days]
// By default, a 'media' directory will be created in the bucket and used as the root directory for all media uploads
// <method> : 'all' or 'updated' (default)
// <days>   : used with method, number of days to check back
program
	.command('media [method] [days]') // TODO: improve description
	.description('\n\Backup media folders, uploads all files to the remote S3 bucket'.cyan)
	.action(function(method, days) {
		nibu.uploadMedia(method, days);
	});

// nibu mysql [useLocalBackupDir]
program
	.command('mysql')
	.description('\n\Backup mysql databases'.cyan)
	.action(function() {
		nibu.uploadMysql();
	});

// nibu backupDb <databaseName>
// <databaseName> : the name of the database to backup
// Backs up the specified database to {bucketName}/mysql backups/YYYY/MM/DD/dbName
// i.e. test.vm.newicon.net/mysql backups/2015/01/08/permagard_test.sql.gz
program
	.command('backupDb <dbName>')
	.description('\n\
    Backup a single database \n\
	<dbName> : the name of the database to backup. \n\
	The database backup file will be uploaded to {bucketName}/mysql backups/YYYY/MM/DD/dbName \n\
	e.g. test.vm.newicon.net/mysql backups/2015/01/08/permagard_test.sql.gz'.cyan)
	.action(function(dbName) {
		nibu.backupDb(dbName, nibu.exitProcess);
	});

// nibu createBucket bucketName
// <bucketName> : the name of the bucket you want to create
program
	.command('createBucket <bucketName>')
	.description('\n\
    Create a new bucket on AWS S3. \n\
    <bucketName> : the name of the bucket you want to create'.cyan)
	.action(function(bucketName) {
		nibu.createBucket(bucketName);
	});

// nibu uploadFile <filePath> [remotePath]
// <filePath> : file to be uploaded (including local file path)
// <remotePath> : if set, the file will use this; otherwise, it will look for an equivalent directory 
program
	.command('uploadFile <filePath> [remotePath]')
	.description('\n\
    Upload a file to S3, usage is similar to the "cp" command\n\
    <filePath>  : file to be uploaded (including local file path)\n\
    [remotePath] : if set, the file will use this as the aws S3 remote file path (including file name); otherwise it will use the file path variable'.cyan)
	.action(function(filePath, remoteDir) {
		nibu.uploadSingleFile(filePath, remoteDir);
	});

// nibu uploadDir fileName [remoteDir]
// <filePath> : file to be uploaded (including local file path)
// <remoteDir> : if set, the file will use this; otherwise, it will look for an equivalent directory
program
	.command('uploadDir <localDir> <remoteDir>')
	.description('\n\
    Upload a directory to S3\n\
    <localDir>  : file to be uploaded (including local file path)\n\
    <remoteDir> : The coresponding remote directory'.cyan)
	.action(function(localDir, remoteDir) {
		nibu.uploadDir(localDir, remoteDir);
	});

// nibu decryptBackup fileName [outputDir]
// <fileName> : the file to decrypt (including path)
// <outputDir> : if set, the file will output to this directory; otherwise, it will use the original file's directory
program
	.command('decryptBackup <filePath> [outputDir]')
	.description('\n\
	Decrypt a local backup file\n\
    <filePath>  : the file to decrypt (including path), \n\
    [outputDir] : if set, the file will output to this directory; otherwise, it will use the original file\'s directory'.cyan)
	.action(function(filePath, outputDir) {
		promptly.password('Type the encryptioin password: ', function(err, password) {
			nibu.decryptToFile(filePath, password, outputDir);
		});
	});

// nibu importDb fileName dbName
// fileName : file to import (including path)
// dbName : the database to use for the import
program
	.command('importDb <filePath> <dbName>')
	.description('\n\
    Import a database: \n\
    <filePath> : file to import (including path),\n\
    <dbName>   : the database to use for the import'.cyan)
	.action(function(filePath, dbName) {
		nibu.importDb(filePath, dbName);
	});

// nibu importDbFromS3 objectKey dbName
// objectKey : S3 object key to use for import (including path)
// dbName : the database to use for the import
program
	.command('importDbFromS3 <objectKey> <dbName>')
	.description('\n\
    Import Database from S3 \n\
	<objectKey> : S3 object key to use for import (including path) <dbName>    : the database to use for the import'.cyan)
	.action(function(objectKey, dbName) {
		nibu.importDbFromS3(objectKey, dbName);
	});

// nibu downloadMediaFromS3 remotePath localPath
// <remotePath> : Source path - the directory path on amazon following the media folder inside the bucket
// <localPath> : Destination path - local folder path where the files will be downloaded
program
	.command('downloadMediaFromS3 <remotePath> <localPath>')
	.description('\n\
	Downloads media from S3 \n\
	<remotePath> Source path - the directory path on amazon following the media folder inside the bucket\n\
	<localPath> Destination path - local folder path where the files will be downloaded '.cyan)
	.action(function(remotePath, localPath) {
		console.log('Downloading media');
		nibu.downloadMediaFromS3(remotePath, localPath);
	});

// show the current config used by nibu
// this could also be shown in a . syntax e.g. "s3.defaults.option"
// then it would be possible to set options via a command
// e.g. nibu setconfig <option> <value>
program
	.command('config')
	.description('\n\
    Shows your current config'.cyan)
	.action(function(option, value) {
		var config = nibu.getConfig();
		console.log(JSON.stringify(config, null, 2));
	});

program
	.command('login')
	.action(function(option, value) {
		console.log('login');
		program.prompt('name: ', function(name) {
			console.log('hi %s', name);
		});
	});

program.parse(process.argv);

// display help if no commands given to nibu
if (!program.args.length) {
	program.outputHelp(function(text) {
		var asciArt = "\n";
		asciArt += "    ███╗   ██╗██╗██████╗ ██╗   ██╗ \n";
		asciArt += "    ████╗  ██║██║██╔══██╗██║   ██║ \n";
		asciArt += "    ██╔██╗ ██║██║██████╔╝██║   ██║ \n";
		asciArt += "    ██║╚██╗██║██║██╔══██╗██║   ██║ \n";
		asciArt += "    ██║ ╚████║██║██████╔╝╚██████╔╝ \n";
		asciArt += "    ╚═╝  ╚═══╝╚═╝╚═════╝  ╚═════╝  \n";
		var out = asciArt + "\n" + text + "\n";
		if (!nibu.hasConfig()) {
			out += nibu.configError() + "\n";
		}
		return out;
	});
}