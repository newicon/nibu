#!/usr/bin/env node
var homeDir = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/';
var configFile = homeDir+'.nibu/config.json';
var chokidar = require('chokidar'),
	 fs = require('fs')
	 _ = require('underscore'),
	 shell = require('shelljs');

if (fs.existsSync(homeDir+'/.nibu/config.json') == false) {
	console.log('ERROR: Couldn\'t find config file: ['+configFile+']');
	console.log('       Please create config file and try again.');
	console.log('       See ['+process.cwd()+'/config.example.json] for info.');
	process.exit(-1);
}

var watcher = {
	start: function(path){
		chokidar.watch(path, {persistent: true, ignored: /[\/\\]\./}).on('all', function(event, path) {
			if (typeof(watcher[event]) == 'function') {
				watcher[event](path);
			}
		});
	},
	add: function(path){
		shell.exec('nibu uploadFile '+path);
	},
	change: function(path){
		shell.exec('nibu uploadFile '+path);
	}
};

var config = require(configFile);

_.each(config.media.directories, function(localDir, remoteDir){
	watcher.start(localDir);
});