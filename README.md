NIBU - Newicon Backup
=======================


A simple database backup software tool written in node.js by [Newicon](newicon.net)  
Nibu can take database dumps on a schedule and upload them to an aws S3 bucket, it can also encrypt the data before uploading.


##Repo

https://bitbucket.org/newicon/nibu/overview

##Installation


- Install globally:


    npm install nibu -g

- Add configuration file ~/.nibu/config.json
  
  _You can find an example file in this package called "config.example.json"_

##Usage

Basic usage:

    nibu command [params]

###List of commands:

  
    nibu media [method] [days]
  
 By default, a 'media' directory will be created in the bucket and used as the root directory for all media uploads
 - _[method] : 'all' or 'updated' (default)_
 - _[days] : used with method, number of days to check back_
    
    nibu mysql

Will backup all databases nibu has access to via its db user credentials specified in the configs db settings


    nibu createBucket bucketName
	
- _bucketName : the name of the bucket you want to create_

    nibu uploadFile fileName [remoteDir]
    
- _filePath : file to be uploaded (including local file path)_
- _[remoteDir] : if set, the file will use this as the aws S3 remote path; otherwise it will use a path similar to the filePath variable

    
    nibu decryptBackup fileName [outputDir]
    
- _fileName : the file to decrypt (including path)_
- _[outputDir] : if set, the file will output to this directory; otherwise, it will use the original file's directory_
 

    nibu importDb fileName dbName

- _fileName : file to import (including path)_
- _dbName : the database to use for the import_
    

    nibu importDbFromS3 objectKey dbName

- _objectKey : S3 object key to use for import (including path)_
- _dbName : the database to use for the import_


    nibu all

This is run without any options and uses the defaults only. Future releases may include options...

    nibu config

Show current config options

    nibu -V

Show the current nibu version

##Configuration Options

- **s3** _(all options for s3)_
 - **defaults** _(access credentials)_
   - **accessKeyId**: your public access key
   - **secretAccessKey**: your private/secret access key
   - **endpoint**: the endpoint for your account _e.g. 's3-eu-west-1.amazonaws.com'_
   - **region**: the region for your account _e.g. 'eu-west-1'_
  - **bucketName**: your bucket name
  - **maxBufferLength**: the maximum file buffer size before multipart uploads kick in; _default_: '1073741824'
  - **multipart**: _(options for multipart uploads)_
   - **partSize**: size of each part or 'chunk' in bytes_; _default: '5242880'_ (5mb)
   - **maxUploadTries**: the maximum number of upload attempts per part before it errors; _default: 3_
 - **db** _(db configuration options)_
   - **username**
   - **password**: if set, this is also used for backup encryptions
   - **encrypt_backup**: boolean; whether to use ssl encryption for your db backups. If set to true, you _must_ use a db password, as this is used for the encryption key. Future releases may allow a different key...
- **media** _(options for media backups)_
- **directories**: list of directories to use as 'roots' for backing up media. Example:

    {
        "/path/to/local/directory/" : "path/to/remote/media/directory/",
        "/path/to/another/local/directory/" : "path/to/another/remote/media/directory/"
    }
 - **excludedDirectories**: array of directories to exclude, such as cache, assets etc Example:

    ["assets", "bundle", "cached-copy", "runtime"]
 

You can find an example configuration file in the root of this package, called **config.example.json**

***newicon.net***