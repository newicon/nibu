var homeDir = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/';
var configFile = homeDir + '.nibu/config.json';
var fs = require('fs');
var path = require('path');
var dir = require('node-dir');
var _ = require('underscore');
var aws = require('aws-sdk');
var moment = require('moment');
var shell = require('shelljs');
var mysql = require('mysql');
var mysqlUtils = require('mysql-utilities');
var FindFiles = require('node-find-files');
var colors = require('colors');
var mime = require('mime-types');
var startTime = new Date();
var async = require('async');

var config = {
	db: {},
	s3: {}
};

// increase the sockets increases bandwidth and makes the upload faster
// https://github.com/andrewrk/node-s3-client#tips
var http = require('http');
var https = require('https');
http.globalAgent.maxSockets = https.globalAgent.maxSockets = 20;


var configError = function() {
	var out = colors.red.bold('ERROR: Couldn\'t find config file: [' + configFile + ']');
	out += colors.red.bold("\n    " + 'Please create config file and try again.');
	out += colors.red.bold("\n    " + 'See [' + process.cwd() + '/config.example.json] for info.');
	out += "\n";
	return out;
}

// check config
if (fs.existsSync(homeDir + '/.nibu/config.json') == false) {
	// process.exit(-1);
} else {
	var config = require(configFile);
}

var s3 = new aws.S3(config.s3.defaults);

var nibu = {
	/**
	 * Whether we have a config file set up
	 */
	hasConfig: function() {
		return fs.existsSync(homeDir + '/.nibu/config.json');
	},

	configError: function() {
		return configError();
	},

	// also check config.s3.db for versions < 0.4.3
	db: config.db || (config.s3.db || {}),
	processCount: 1,
	getConfig: function() {
		return config;
	},
	uploadMysql: function() {
		if (_.isUndefined(nibu.db.username)) {
			console.log("ERROR: No database user set.")
			process.exit(-1);
		}
		var connection = nibu.dbConnection();
		mysqlUtils.introspection(connection);
		mysqlUtils.upgrade(connection);
		connection.connect();
		connection.databases(function(err, dbNames) {
			// filter the databases to backup
			var databases = _.without(dbNames,
				'information_schema',
				'mysql',
				'performance_schema',
				'phpmyadmin'
			);
			// backup each database
			var message = databases.length + ' Databases found.';
			console.log(message.cyan);

			async.eachSeries(databases, function iteratee(dbName, callback) {
				nibu.backupDb(dbName, callback);
			});
		});
		connection.end();
	},

	getMysqlBackupFolder: function() {
		return 'mysql backups/' + moment().format('YYYY/MM/DD');
	},

	/**
	 * Backs up the specified database to {bucketName}/mysql backups/YYYY-MM-DD/dbName
	 * i.e. test.vm.newicon.net/mysql backups/2015-01-08/permagard_test.sql.gz
	 * @param {string} dbName The name of the database to backup
	 * @param {function} callback Callback function
	 * @return void
	 */
	backupDb: function(dbName, callback) {

		console.log("Running mysqldump of [" + dbName + "]");
		var fileName = dbName + '.sql.gz';
		var gzipCommand = 'gzip';
		if (nibu.db.encrypt_backup == 'true' && !_.isUndefined(nibu.db.password)) {
			gzipCommand += ' | openssl enc -aes-256-cbc -e -k ' + nibu.db.password;
			fileName += '.enc';
		}
		var fileNameFull = homeDir + fileName;

		var completedMysqlDump = function(code, output) {
			console.log("Mysqldump complete. Database [" + dbName + "] stored locally at [" + fileNameFull + "]");
			nibu.uploadFile(nibu.getMysqlBackupFolder() + '/' + fileName, fileNameFull, completedMysqlUpload);
		}

		var completedMysqlUpload = function() {
			fs.unlink(fileNameFull);
			console.log("Local backup file [" + fileNameFull + "] deleted following upload");
			console.log("------------------------------------------------");
			if (_.isFunction(callback)) {
				callback();
			}
		}

		shell.exec('mysqldump -u ' + nibu.db.username + (nibu.db.password ? ' -p' + nibu.db.password : '') + ' ' + dbName + ' | ' + gzipCommand + ' > ' + fileNameFull, completedMysqlDump);
	},

	uploadDir: function(localDir, remoteDir, excludeDirs) {
		var stat = fs.statSync(localDir);
		if (stat.isFile()) {
			throw "The localDir: '".red + localDir.red + "' specified is not a directory".red;
		}
		var options = {
			recursive: true
		};
		var excludedDirs = excludeDirs || null;
		console.log("Uploading files in [" + localDir + "]");
		options.excludeDir = excludedDirs;
		dir.readFiles(localDir, options, function(err, content, filePath, next) {
			remoteDir = path.normalize(remoteDir + '/'); // make sure remoteDir has a trailing slash
			var remotePath = filePath.replace(localDir, remoteDir);
			nibu.uploadFile(remotePath, filePath, next);
		});
	},
	uploadMedia: function() {
		var mediaDirectories = config.media.directories;
		var excludedDirs = config.media.excludedDirectories || null;
		_.each(mediaDirectories, function(remoteDir, localDir) {
			console.log("Uploading files in [" + localDir + "]");
			var options = {recursive: true};
			if (!_.isNull(excludedDirs))
				options.excludeDir = excludedDirs;
			dir.readFiles(localDir, options, function(err, content, filePath, next) {
				nibu.processFile(filePath, localDir, remoteDir, next);
			});
		});
	},
	// needs to be refactored... Could just use uploadFile function
	processFile: function(file, localDir, remoteDir, next) {
		if (typeof(file) == 'object') {
			var stats = file.stats;
			var filePath = file.filePath;
		} else {
			var filePath = file;
			var stats = fs.statSync(filePath);
		}
		var uploadFileName = 'media/' + filePath.replace(localDir, remoteDir);

		// skip directories
		if (!stats.isFile()) {
			if (_.isFunction(next)) next();
			return;
		}

		s3.getObject({
			Bucket: config.s3.bucketName,
			Key: uploadFileName,
			IfModifiedSince: stats.mtime
		}, function(err, data) {
			if (err) {
				if (err.code == 'NoSuchBucket') {
					nibu.catchS3Error(err);
				}
				// the file does not exist on S3
				// This is a valid error that we can expect
				// Upload!
				else if (err.code == "NoSuchKey") {
					fs.readFile(filePath, function(err, content) {
						if (err) {
							console.log(err);
							console.log('Could not read file "' + filePath + '"');
							next();
						} else {
							nibu.uploadFile(uploadFileName, filePath, next);
						}
					});
					// File has not been modified
				} else if (err.code == "NotModified") {
					nibu.uploadFile(uploadFileName, filePath, next);
					// An unknown error
				} else {
					console.log('Error!');
					console.log(err);
					next();
				}
			} else {
				console.log("Already up-to-date: [" + filePath + "] => [" + uploadFileName + "]");
				if (_.isFunction(next)) next();
			}
		});
	},
	uploadAll: function() {
		nibu.uploadMysql();
		nibu.uploadMedia();
	},
	uploadSingleFile: function(filePath, remotePath) {
		var stat = fs.statSync(filePath);
		if (!stat.isFile()) {
			throw "The local filePath: '".red + filePath.red + "' specified is not a file".red;
		}
		// use the same file path as local if not specified
		remotePath = remotePath || filePath;
		nibu.uploadFile(remotePath, filePath);
	},
	formatRemotePath: function(remoteFilePath) {
		var out = remoteFilePath.replace(/\.\.\//g, '');
		out = out.replace(/\.\//g, '');
		// remove beggining slash
		if (out[0] == "/") {
			out = out.substr(1, out.length);
		}
		// remove trailing slash
		if (out[out.length - 1] == "/") {
			out = out.substr(0, out.length - 1);
		}
		return path.normalize(out);
	},
	uploadFile: function(remoteFilePath, filePath, successCallback, errorCallback) {
		var metaData = nibu.getFileMimeType(filePath);
		var fs = require('fs');

		remoteFilePath = nibu.formatRemotePath(remoteFilePath);

		var params = {
			ACL: 'private',
			Bucket: config.s3.bucketName,
			Key: remoteFilePath,
			Body: fs.createReadStream(filePath),
			ContentType: metaData,
			partSize: 10 * 1024 * 1024,
			queueSize: 1
		};

		console.log('Uploading file [' + filePath + '] to [' + path.normalize(config.s3.bucketName + '/' + remoteFilePath) + '] as [' + metaData + ']');

		var uploaded = function(err, data) {
			params = null;
			if (err) {
				if (!_.isUndefined(errorCallback)) {
					errorCallback(err)
				}
				console.log(err, data);
			} else {
				console.log(' - Successfull\n'.green);
				// console.log('----------------------------');
				if (!_.isUndefined(successCallback)) {
					params = null;
					successCallback();
				}
			}
		};

		var progress = function(evt) {
			process.stdout.clearLine();
			process.stdout.cursorTo(0);
			process.stdout.write('└─ Progress ' + evt.loaded + '/' + evt.total);
		};

		var end = function() {
			console.log("done uploading");
		};

		// do the upload
		s3.upload(params, uploaded)
			.on('httpUploadProgress', progress);

	},
	getFileList: function(path) {
		var i, fileInfo, filesFound;
		var fileList = [];
		filesFound = fs.readdirSync(path);
		for (i = 0; i < filesFound.length; i++) {
			fileInfo = fs.lstatSync(path + filesFound[i]);
			if (fileInfo.isFile())
				fileList.push(filesFound[i]);
		}
		return fileList;
	},
	getFileMimeType: function(fileName) {
		var type = mime.lookup(fileName) || 'application/octet-stream';
		return type;
	},
	createBucket: function(bucketName, callback) {
		s3.createBucket({
			Bucket: bucketName
		}, function(callback) {
			console.log('Created the bucket[' + bucketName + ']')
			console.log(arguments);
			if (!_.isUndefined(callback) && typeof(callback) == 'function')
				callback();
		});
	},
	// this is awfull programming!
	// should not be needed!
	exitProcess: function(exitCode) {
		nibu.processCount--;
		if (nibu.processCount <= 0) {
			if (_.isUndefined(exitCode))
				var exitCode = 0;
			console.log('Finished');
			process.exit(exitCode);
		}
		throw 'This function is deprecated and horrible';
	},
	dbConnection: function(dbName) {
		nibu.db.host = nibu.db.host || 'localhost';
		// perhaps this should be renamed to user to be consistent in the config file
		nibu.db.user = nibu.db.username;
		nibu.db.password = nibu.db.password || null;
		if (!_.isUndefined(dbName))
			params.database_name = dbName;
		return mysql.createConnection(nibu.db);
	},
	decryptToFile: function(filePath, password, outputDir) {
		if (!_.isUndefined(outputDir)) {
			fileName = path.basename(filePath);
			var nonEncFilename = outputDir + '/' + fileName.replace('.enc', '');
		} else {
			var nonEncFilename = filePath.replace('.enc', '');
		}
		shell.exec('openssl enc -aes-256-cbc -d -k ' + password + ' -in ' + filePath + ' -out ' + nonEncFilename, function(code, output) {
			// code 0 is success
			if (code == 0) {
				console.log("Decrypted file [" + filePath + "] to [" + nonEncFilename + "]");
			} else {
				console.log("Decryption failed, Bad decrypt, The supplied password was probably incorrect".red);
			}
			nibu.exitProcess();
		});
	},
	importDb: function(filePath, dbName, callback) {
		fs.exists(filePath, function(exists) {
			if (exists) {
				if (nibu.db.encrypt_backup == 'true' && filePath.indexOf('.enc') >= 0) {
					shell.exec('openssl enc -aes-256-cbc -d -k ' + nibu.db.password + ' -in ' + filePath + ' | gunzip | mysql -u ' + nibu.db.username + ' -p' + nibu.db.password + ' ' + dbName, function() {
						if (!_.isUndefined(callback))
							callback();
					});
				} else {
					shell.exec('mysql -u ' + nibu.db.username + ' -p' + nibu.db.password + ' ' + dbName + ' < gunzip ' + filePath, function() {
						if (!_.isUndefined(callback))
							callback();
					});
				}
			} else {
				console.log("ERROR: File does not exist locally.");
				console.log("       To run locally, first please upload/download file to the server.");
				console.log("       To run from S3, use command 'importDbFromS3' with params [fileKey] and [dbName]");
				process.exit(-1);
			}
		});
	},
	importDbFromS3: function(fileKey, dbName) {
		nibu.getObject(fileKey, function(data) {
			var fileName = path.basename(fileKey);
			var filePath = '/tmp/dbbackup_' + moment().format('YYYY-MM-DD_hmmss') + '_' + fileName;
			fs.writeFile(filePath, data.Body, function(err) {
				if (err)
					throw err;
				nibu.importDb(filePath, dbName, function() {
					nibu.exitProcess();
				});
				return;
			});
		})
	},
	/**
	 * downloadMediaFromS3 - Download media from Amazon S3 for the required site
	 * @param  {string} remotePath Remote Amazon path following the media folder
	 * @param  {string} localPath  local folder path to the sytem
	 */
	downloadMediaFromS3: function(remotePath, localPath) {
		if (_.isUndefined(localPath) || _.isUndefined(remotePath)) {
			console.log("ERROR: Please provide the local/remote path.".red)
			process.exit(-1);
		}
		if (!fs.existsSync(localPath)) {
			console.log("ERROR: Local path doesn not exist.".red);
			process.exit(-1);
		}

		// Check API - //github.com/andrewrk/node-s3-client
		var s3lib = require('s3');
		// Create client for Amazon S3
		var client = s3lib.createClient({
			s3Client: s3,
			maxAsyncS3: 20, // this is the default
			s3RetryCount: 3, // this is the default
			s3RetryDelay: 1000, // this is the default
			multipartUploadThreshold: 20971520, // this is the default (20 MB)
			multipartUploadSize: 15728640, // this is the default (15 MB)
		});
		//  Configure params to download the media from the bucket
		var params = {
			localDir: localPath,
			deleteRemoved: false, // default false, whether to remove s3 objects that have no corresponding local file.
			s3Params: {
				Prefix: path.normalize('media/' + remotePath + '/'),
				Bucket: config.s3.bucketName
			}
		};
		// Call the downloader with the required params
		var downloader = client.downloadDir(params);

		// Listen to error event
		downloader.on('error', function(err) {
			console.error("unable to sync:", err.stack);
		});

		// Show the progress report
		downloader.on('progress', function() {
			process.stdout.clearLine();
			process.stdout.cursorTo(0);
			var out = '└─ Progress ' + downloader.progressAmount + '/' + downloader.progressTotal;
			process.stdout.write(out);
		});

		// Display 'complete' message once the job is done - listen to end event
		downloader.on('end', function() {
			console.log("\n└─ completed");
		});

	},
	getObject: function(fileKey, callback) {
		s3.getObject({
			Key: fileKey,
			Bucket: config.s3.bucketName
		}, function(err, data) {
			if (err)
				nibu.catchS3Error(err);
			if (!_.isNull(data)) {
				if (!_.isUndefined(callback))
					callback(data);
				else
					return data;
			}
		});
	},
	catchS3Error: function(err) {
		switch (err.code) {
			case 'NoSuchBucket':
				console.error('Bucket [' + config.s3.bucketName + '] does not exist');
				nibu.createBucket(config.s3.bucketName, function() {
					console.log('Please try again...');
					process.exit(-1);
				});
				break;
			default:
				console.log("ERROR: File does not exist on S3.");
				process.exit(-1);
		}
	}
}

module.exports = nibu;